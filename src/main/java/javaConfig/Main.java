package javaConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        System.out.println("Test");
        ApplicationContext context =
                new AnnotationConfigApplicationContext("javaConfig");
        Cat catBean = context.getBean(Cat.class);
        Dog dogBean = context.getBean(Dog.class);
        catBean.say();
        dogBean.say();
        System.out.println("They now say from home");
       House houseBean = context.getBean(House.class);
       houseBean.method();
    }
}
