package configByXml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Dog beanDog = context.getBean(Dog.class);
        Cat beanCat = context.getBean(Cat.class);
        beanDog.say();
        beanCat.say();
        System.out.println("They now say from home");
        House beanHouse = context.getBean(House.class);
        beanHouse.method();
    }
}
