package annotationConfig;

import org.springframework.stereotype.Component;

@Component
public class Cat implements Animal {
    @Override
    public void say() {
        System.out.println("Meow by annotations");
    }
}
