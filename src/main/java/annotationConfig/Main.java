package annotationConfig;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(
                "annotationConfig");
        House bean = annotationConfigApplicationContext.getBean(House.class);
        bean.method();
    }
}
